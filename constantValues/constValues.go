package main

import (
	"fmt"
	"math"
)

func main() {
	const pi float64 = 3.14
	var alan float64
	var yaricap float64
	fmt.Println("daire alanı için yarıçap giriniz:")
	fmt.Scanf("%f", &yaricap)
	alan = math.Pow(yaricap, 2) * pi
	fmt.Println("alan=> ", alan)
}
