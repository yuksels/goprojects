package main

import (
	"fmt"
)

func main() {
	var arr [5]int
	fmt.Println(arr)
	fmt.Println("array length=> ", len(arr))

	for i := 0; i < len(arr); i++ {
		arr[i] = i
		fmt.Println(arr)
		fmt.Println(arr[i])

	}

	matrix := [4][4]int{
		{0, 0, 0, 0},
		{0, 0, 0, 0},
		{0, 0, 0, 0},
		{0, 0, 0, 0}}
	fmt.Println(matrix)
}
