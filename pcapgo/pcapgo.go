package main

import (
	"fmt"
	"log"
	"strings"
	"time"

	"github.com/google/gopacket"
	"github.com/google/gopacket/layers"
	"github.com/google/gopacket/pcap"
)

var (
	device            = "wlo1"
	snapshotlen int32 = 1500
	promiscuous       = true
	err         error
	timeout     = -1 * time.Second
	handle      *pcap.Handle
)

func printEthernetInfo(packet gopacket.Packet) {
	ethernetLayer := packet.Layer(layers.LayerTypeEthernet)
	if ethernetLayer != nil {
		fmt.Println("Ethernet layer detected.")
		ethernetPacket, _ := ethernetLayer.(*layers.Ethernet)
		fmt.Println("Source MAC: ", ethernetPacket.SrcMAC)
		fmt.Println("Destination MAC: ", ethernetPacket.DstMAC)
		// Ethernet type is typically IPv4 but could be ARP or other
		fmt.Println("Ethernet type: ", ethernetPacket.EthernetType)
		fmt.Println()
	}
}

func printIPInfo(packet gopacket.Packet) {
	ipLayer := packet.Layer(layers.LayerTypeIPv4)
	if ipLayer != nil {
		fmt.Println("IPv4 layer detected.")
		ip, _ := ipLayer.(*layers.IPv4)

		// IP layer variables:
		// Version (Either 4 or 6)
		// IHL (IP Header Length in 32-bit words)
		// TOS, Length, Id, Flags, FragOffset, TTL, Protocol (TCP?),
		// Checksum, SrcIP, DstIP
		fmt.Printf("From %s to %s\n", ip.SrcIP, ip.DstIP)
		fmt.Println("Protocol: ", ip.Protocol)
		fmt.Println()
	}
}

func printTCPInfo(packet gopacket.Packet) {
	tcpLayer := packet.Layer(layers.LayerTypeTCP)
	if tcpLayer != nil {
		fmt.Println("TCP layer detected.")
		tcp, _ := tcpLayer.(*layers.TCP)

		// TCP layer variables:
		// SrcPort, DstPort, Seq, Ack, DataOffset, Window, Checksum, Urgent
		// Bool flags: FIN, SYN, RST, PSH, ACK, URG, ECE, CWR, NS
		fmt.Printf("From port %d to %d\n", tcp.SrcPort, tcp.DstPort)
		fmt.Println("Sequence number: ", tcp.Seq)
		fmt.Println()
	}
}

func printPacketInfo(packet gopacket.Packet) {

	// Iterate over all layers, printing out each layer type
	fmt.Println("All packet layers:")
	for _, layer := range packet.Layers() {
		fmt.Println("- ", layer.LayerType())
	}

	// When iterating through packet.Layers() above,
	// if it lists Payload layer then that is the same as
	// this applicationLayer. applicationLayer contains the payload
	applicationLayer := packet.ApplicationLayer()
	if applicationLayer != nil {
		fmt.Println("Application layer/Payload found.")
		fmt.Printf("%s\n", applicationLayer.Payload())

		// Search for a string inside the payload
		if strings.Contains(string(applicationLayer.Payload()), "HTTP") {
			fmt.Println("HTTP found!")
		}
	}

	// Check for errors
	if err := packet.ErrorLayer(); err != nil {
		fmt.Println("Error decoding some part of the packet:", err)
	}
}

type filterEther struct {
	Srcmac *string
	Dstmac *string
}
type filterIP struct {
	Srcip    *string
	Dstip    *string
	Protocol *string
}

type filterTCP struct {
	Srcport *uint16
	Dstport *uint16
}

type filterUDP struct {
	Srcport *uint16
	Dstport *uint16
}
type filterApplication struct {
	Length *int32
}

type filter struct {
	Ether       *filterEther
	IP          *filterIP
	TCP         *filterTCP
	UDP         *filterUDP
	Application *filterApplication
}

func filterPacket(packet gopacket.Packet, filt *filter) bool {
	if filt == nil {
		return false
	}

	if filt.Ether != nil {

	}

	if filt.IP != nil {
		ipLayer := packet.Layer(layers.LayerTypeIPv4)
		if ipLayer != nil {
			ip, _ := ipLayer.(*layers.IPv4)
			if filt.IP.Srcip != nil && *filt.IP.Srcip != ip.SrcIP.String() {
				return true
			}
			if filt.IP.Dstip != nil && *filt.IP.Dstip != ip.DstIP.String() {
				return true
			}
			if filt.IP.Protocol != nil && *filt.IP.Protocol != ip.Protocol.String() {
				return true
			}
		}
	}

	if filt.TCP != nil {

	}

	if filt.UDP != nil {

	}

	if filt.Application != nil {

	}

	return false
}

func main() {
	// Open device
	handle, err = pcap.OpenLive(device, snapshotlen, promiscuous, timeout)
	if err != nil {
		log.Fatal(err)
	}
	defer handle.Close()

	fmt.Println("Starting capturing")
	// Use the handle as a packet source to process all packets
	packetSource := gopacket.NewPacketSource(handle, handle.LinkType())
	for packet := range packetSource.Packets() {
		// Process packet here
		srcip := "10.5.178.147"
		var filt filter
		filt.IP = &filterIP{Srcip: &srcip}
		if filterPacket(packet, &filt) == false {
			printEthernetInfo(packet)
			printIPInfo(packet)
			printTCPInfo(packet)
			printPacketInfo(packet)
		}
	}
}
