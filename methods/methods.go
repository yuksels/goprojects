// methods.go
package main

import (
	"fmt"
)

type rect struct {
	width, height int
}

func (r rect) perim() int {
	return 2*r.height + 2*r.width
}

func (r *rect) area() int {
	return r.height * r.width
}

func main() {
	r := rect{width: 2, height: 3}
	var prm int = r.perim()
	fmt.Println("çevre==>> ", prm)
	r1 := &r
	alan := r1.area()
	fmt.Println("alan==>> ", alan)

}
