package main

import (
	"fmt"
	"time"
)

func main() {
	start := time.Now()
	fmt.Println(start)
	time.Sleep(time.Second * 10)
	stop := time.Now()
	diff := stop.Sub(start)
	fmt.Println(stop)
	fmt.Println(diff)
}
