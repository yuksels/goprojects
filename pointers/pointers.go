package main

import (
	"fmt"
)

func main() {
	i := 1
	var ptr *int
	ptr = &i
	fmt.Println(i)
	fmt.Println(&i)
	fmt.Println(*ptr)
	fmt.Println(ptr)
}
