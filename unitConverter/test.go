// test.go
package main

import (
	"fmt"
	"unitConverter"
)

func main() {
	fmt.Println("Hello World!")
	tmp := unitConverter.BitToByte(12)
	fmt.Println(tmp)
	tmp = unitConverter.ByteToBit(tmp)
	fmt.Println(tmp, " ", 8.0/10.0)
}
