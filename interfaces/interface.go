// interface
package main

import (
	"fmt"
	"math"
)

const pi = 3.14

type geom interface {
	area() float64
	perim() float64
}
type rect struct {
	width, height float64
}
type circle struct {
	radius float64
}

func (r rect) area() float64 {
	return r.width * r.height
}
func (r rect) perim() float64 {
	return 2 * (r.height + r.width)
}
func (c circle) area() float64 {
	return math.Pi * math.Pow(c.radius, 2)
}
func (c circle) perim() float64 {
	return 2 * math.Pi * c.radius
}

func measure(g geom) {
	fmt.Println(g)
	fmt.Println(g.area())
	fmt.Println(g.perim())
}
func main() {
	daire := circle{radius: 5.3}
	kare := rect{height: 4.45, width: 3.453}
	fmt.Println(daire.area())
	measure(daire)
	measure(kare)
}
