package main

import (
	"fmt"
)

func main() {

	message := make(chan int32)
	go func() { message <- -2147483647 }()
	msg := <-message
	var tmp int32
	tmp2 :=
		fmt.Println(msg)
}
