package main

import (
	"fmt"
)

func main() {
	var a, b int
	var varBool bool
	b = 12
	var varString string
	var varString2 string = "varstring"
	varInit := true
	fmt.Println("a=> ", a, "\nb=> ", b, "\nvarBool=> ", varBool, "\nvarString=> ", varString, "\nvarString2=> ", varString2, "\nvarInit=> ", varInit)
}
