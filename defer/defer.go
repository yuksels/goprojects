package main

import (
	"fmt"
	"os"
)

func main() {
	saveToFile("sometext.txt", "this is gonna be the best day of my life")
}

func saveToFile(name string, content string) {
	fmt.Println("creating...")
	file, error := os.Create(name)
	if error == nil {
		//defer closeFile(file) // dogrudan defer file.Close() da denenmeli
		//	file.Close()
		fmt.Fprintln(file, content)
	} else {
		return
	}
}

func closeFile(file *os.File) {
	fmt.Println("closing...")
	file.Close()
	fmt.Println("closed")
}
