package main

import (
	"fmt"
	"math"
)

func main() {

	for i := 0; ; i++ {

		if i%2 == 0 {
			fmt.Println("i=>", i)
			continue
		}
		if math.Pow((float64)(i), 3) >= 216 {
			fmt.Println("loop is over\ni=>", i, "i^3=>", math.Pow((float64)(i), 3))
			break
		}

	}
}
