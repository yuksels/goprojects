package main

import (
	"fmt"
)

func main() {
	slice := make([]string, 8)
	slice2 := make([]string, 3)
	/*	s[0] = "bir"
		fmt.Println("s.len==>> ", len(s))
		s = append(s, "iki")
		fmt.Println(s)
		fmt.Println("s.len==>> ", len(s))
	*/
	for i := 0; i < len(slice); i++ {
		slice[i] = "8"
	}

	slice2[0] = "0"
	slice2[1] = "1"
	slice2[2] = "2"
	slice[0] = "0"
	slice[1] = "1"
	slice[2] = "2"
	slice[3] = "3"
	slice[4] = "4"

	/*fmt.Println(" slice==>>", slice)
	fmt.Println(" slice2==>>", slice2)
	copy(slice, slice2)*/
	fmt.Println(" slice==>>", slice)
	fmt.Println(" slice2==>>", slice2)
	l := slice[:2]
	fmt.Println(l)

	twoD := make([][]int, 3)
	for i := 0; i < 3; i++ {
		innerLen := i + 1
		twoD[i] = make([]int, innerLen)

		for j := 0; j < innerLen; j++ {
			twoD[i][j] = i + j
		}
	}
	fmt.Println("2d: ", twoD)

}
