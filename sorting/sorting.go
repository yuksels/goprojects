package main

import (
	"fmt"
	"sort"
)

func main() {
	arrFloat := []float64{1.2, 123.345234, 66545.4, 13.123, 432.1234, 133.41235, 768678.546345, 345.234}
	arrInt := []int{34, 234345, 4564, 324, 2554676, 5797809, 2, 4, 54, 77, 868, 12, 54, 68766, 2344, 6547, 36}
	arrChar := []string{"s", "6", "q", "p", "i", "z", "a"}
	arrString := []string{"samet", "ali", "furkan", "alihan", "batuhan", "davut"}
	fmt.Println(arrFloat)
	fmt.Println(arrInt)
	fmt.Println(arrChar)
	fmt.Println(arrString)
	sort.Float64s(arrFloat)
	sort.Ints(arrInt)
	sort.Strings(arrChar)
	sort.Strings(arrString)
	fmt.Println(arrFloat)
	fmt.Println(arrInt)
	fmt.Println(arrChar)
	fmt.Println(arrString)
	fmt.Println(sort.StringsAreSorted(arrChar))

	dizi := []int{3, 2, 1}
	fmt.Println(sort.IntsAreSorted(dizi))
	fmt.Println(dizi)
	dizi[0] = dizi[2]
	fmt.Println(dizi)
}
