package main

import (
	"fmt"
)

func main() {
	numbers := []int{1, 2, 3, 5, 4}
	defer easy(numbers)
	numbers[6] = 10
	fmt.Println("have fun")
}
func easy(n []int) {
	if err := recover(); err != nil {
		fmt.Printf("slice length is : %d\n", len(n))
		fmt.Printf("don't panic it's just an error\n%s", err)
	}
}
