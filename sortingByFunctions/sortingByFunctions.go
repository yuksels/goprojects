package main

import (
	"fmt"
	"sort"
)

var count int = 0

type byLength []string

func (s byLength) Len() int {
	lenVal := "lenval"
	fmt.Println(lenVal)
	return len(s)
}
func (s byLength) Swap(i, j int) {
	swapVal := "swapvall"
	fmt.Println(swapVal)
	s[i], s[j] = s[j], s[i]
}
func (s byLength) Less(i, j int) bool {
	count += 1
	LessVal := "Lessval"
	fmt.Println(LessVal)
	//tmp := len(s[i]) < len(s[j])
	//fmt.Println(tmp)
	return len(s[i]) < len(s[j])
}

func main() {
	fruits := []string{"peach", "banana", "kiwi"}
	sort.Sort(byLength(fruits))
	fmt.Println(fruits, "	", count)
}
